package io.github.alirezamht.book.service;

import io.github.alirezamht.book.model.Book;
import io.github.alirezamht.book.repository.BookRepository;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;

@Component("courseService")
@Transactional
public class BookServiceIm implements BookService {

    private BookRepository bookRepository;

    public BookServiceIm(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    @Override
    public Book getBookById(Long id) {
        return bookRepository.getBookByID(id);
    }

    @Override
    public ArrayList<Book> getAllBook() {
        return bookRepository.getAllBy();
    }


}
