package io.github.alirezamht.book.service;

import io.github.alirezamht.book.model.User;
import io.github.alirezamht.book.repository.UserRepository;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Component("studentService")
@Transactional
public class UserServiceImp implements UserService {

    private final UserRepository userRepository;

    public UserServiceImp(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

}
