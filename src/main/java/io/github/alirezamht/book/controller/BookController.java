package io.github.alirezamht.book.controller;


import io.github.alirezamht.book.model.Book;
import io.github.alirezamht.book.service.BookService;
import io.github.alirezamht.book.util.ResponseFactory;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;


@RestController
@RequestMapping("/book")
public class BookController {
    protected RestTemplate restTemplate = new RestTemplate();

    @Autowired
    private BookService bookService;

    @GetMapping(value ="show" , params = {} )
    public @ResponseBody JSONObject showBook(
           HttpServletResponse response) {
        try {response.setStatus(HttpStatus.OK.value());
                JSONObject result = new JSONObject();
                ArrayList<Book> books  = bookService.getAllBook();
                for (int i = 0 ; i < books.size();i++){
                    result.put(i,books.get(i));
                }
               return ResponseFactory.getSuccessResponse(HttpStatus.OK.value()
                       ,result);

           // return null;
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }
}
