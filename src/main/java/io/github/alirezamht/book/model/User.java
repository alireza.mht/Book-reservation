package io.github.alirezamht.book.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "students")
public class User implements Serializable {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;
    @Column(nullable = false, name= "name")
    private String name;
    @Column(nullable = false, name= "last_name")
    private String lastName;


    public User(String firstName, String lastName, String persianFirstName, String persianLastName, int type, String phoneNumber, int field, String fieldName, String persianFieldName, String nationalNumber, String studentNumber, String password) {

        this.lastName = lastName;

    }

    public User() {
    }

    public Long getId() {
        return id;
    }


    @Override
    public String toString() {
        return "User{" +
                "id='" + id + '\'' +
                '}';
    }

}
