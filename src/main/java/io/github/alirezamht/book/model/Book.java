package io.github.alirezamht.book.model;


import org.json.simple.JSONObject;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "book")
public class Book {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long iD;
    @Column(nullable = false,updatable = false, name = "name")
    private String name;
    @Column(nullable = false, name = "writer")
    private String writer;
    @Column(nullable = false, name = "year")
    private Long year;
    @Column(nullable = false, name = "print_num")
    private Long printNum;
    @Column(nullable = false, name = "isbn_num")
    private Long isbnNum;
    @Column(nullable = false,name = "inventory")
    private Long inventory;
    @Column(nullable = false, name = "photo")
    private String photo;
    @Column(nullable = false,name = "cutsomer_id")
    private Long customerID;





    public Book() {
    }


  public JSONObject getJson(){
      JSONObject object=new JSONObject();
      object.put("ID:" , iD);
      object.put("name:" , name);
      object.put("writer:" , writer);
      object.put("year:" , year);
      object.put("print number:" , printNum);
      object.put("isbn number:" , isbnNum);
      object.put("inventory:" , inventory);
      object.put("photo:" , photo);
      object.put("customer ID:", customerID);
      return object;
  }
}
