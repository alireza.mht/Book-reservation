package io.github.alirezamht.book.repository;

import io.github.alirezamht.book.model.Book;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.lang.reflect.Array;
import java.util.ArrayList;


@Repository
public interface BookRepository extends CrudRepository<Book, Long>  {

     Book getBookByID(Long c);
     //Book getBookByID
     ArrayList<Book> getAllBy();

}
